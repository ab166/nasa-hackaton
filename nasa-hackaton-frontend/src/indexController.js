app.controller('indexController', function ($filter, $scope, $http, $location) {
    // Create a WorldWindow for the canvas.
    $scope.wwd = new WorldWind.WorldWindow("canvasOne");
    var layers = [
        // Imagery layers.
        {layer: new WorldWind.BMNGLayer(), enabled: true},
        {layer: new WorldWind.BMNGLandsatLayer(), enabled: false},
        {layer: new WorldWind.BingAerialLayer(null), enabled: false},
        {layer: new WorldWind.BingAerialWithLabelsLayer(null), enabled: false},
        {layer: new WorldWind.BingRoadsLayer(null), enabled: false},
        // Add atmosphere layer on top of all base layers.
        {layer: new WorldWind.AtmosphereLayer(), enabled: true},
        // WorldWindow UI layers.
        {layer: new WorldWind.CoordinatesDisplayLayer($scope.wwd), enabled: true},
        {layer: new WorldWind.ViewControlsLayer($scope.wwd), enabled: true}

    ];

    for (var l = 0; l < layers.length; l++) {
        layers[l].layer.enabled = layers[l].enabled;
        $scope.wwd.addLayer(layers[l].layer);
    }

    // Web Map Tiling Service information from
    var serviceAddress = "https://tiles.geoservice.dlr.de/service/wmts?SERVICE=WMTS&REQUEST=GetCapabilities&VERSION=1.0.0";
    // Layer displaying Global Hillshade based on GMTED2010
    var layerIdentifier = "hillshade";

    // Called asynchronously to parse and create the WMTS layer
    var createLayer = function (xmlDom) {
        // Create a WmtsCapabilities object from the XML DOM
        var wmtsCapabilities = new WorldWind.WmtsCapabilities(xmlDom);
        // Retrieve a WmtsLayerCapabilities object by the desired layer name
        var wmtsLayerCapabilities = wmtsCapabilities.getLayer(layerIdentifier);
        // Form a configuration object from the WmtsLayerCapabilities object
        var wmtsConfig = WorldWind.WmtsLayer.formLayerConfiguration(wmtsLayerCapabilities);
        // Create the WMTS Layer from the configuration object
        var wmtsLayer = new WorldWind.WmtsLayer(wmtsConfig);

        // Add the layers to WorldWind and update the layer manager
        wwd.addLayer(wmtsLayer);
        layerManager.synchronizeLayerList();
    };


    //Legend on screen
    var screenOffset = new WorldWind.Offset(WorldWind.OFFSET_FRACTION, 1, WorldWind.OFFSET_FRACTION, 0);
        var screenImage1 = new WorldWind.ScreenImage(screenOffset, "C:/Users/PC/Documents/SpaceHackaton/nasa-hackaton/nasa-hackaton-frontend/images/legend.png");

        screenImage1.imageOffset = new WorldWind.Offset(WorldWind.OFFSET_FRACTION, 1, WorldWind.OFFSET_FRACTION, 0);
        screenImage1.imageScale = 0.8;
        var screenImageLayer = new WorldWind.RenderableLayer();
        screenImageLayer.displayName = "Legend";
        screenImageLayer.addRenderable(screenImage1);

    $scope.wwd.addLayer(screenImageLayer);


    
    // Add a placemark
    $scope.placemarkLayer = new WorldWind.RenderableLayer();
    $scope.wwd.addLayer($scope.placemarkLayer);

   

    $scope.placeMark = function (cityName, longitude, latitude, color) {
        var placemarkAttributes = new WorldWind.PlacemarkAttributes(null);

        placemarkAttributes.imageOffset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.3,
            WorldWind.OFFSET_FRACTION, 0.0);
    
        placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.5,
            WorldWind.OFFSET_FRACTION, 1.0);
        placemarkAttributes.imageSource = "https://img.icons8.com/android/48/" + color + "/marker.png";

        var position = new WorldWind.Position(longitude, latitude, 100.0);
        var placemark = new WorldWind.Placemark(position, false, placemarkAttributes);

        placemark.alwaysOnTop = true;
        placemark.label = cityName;

        $scope.placemarkLayer.addRenderable(placemark);
    }

    $scope.logError = function (exception) {
        console.log("There was a failure retrieving the capabilities document: exception: " + exception);
    };

    $scope.shapeConfigurationCallback = function (geometry, properties) {
        var configuration = {};
        var placemarkAttributes = new WorldWind.PlacemarkAttributes(null);

        placemarkAttributes.imageOffset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.3,
            WorldWind.OFFSET_FRACTION, 0.0);
    
        placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.5,
            WorldWind.OFFSET_FRACTION, 1.0);

        if (geometry.isPointType() || geometry.isMultiPointType()) {
            configuration.attributes = new WorldWind.PlacemarkAttributes(placemarkAttributes);

            if (properties && (properties.name || properties.Name || properties.NAME)) {
                configuration.name = properties.name || properties.Name || properties.NAME;
            }
            if (properties && properties.POP_MAX) {
                var population = properties.POP_MAX;
                configuration.attributes.imageScale = 0.01 * Math.log(population);
            }
        }
        else if (geometry.isLineStringType() || geometry.isMultiLineStringType()) {
            configuration.attributes = new WorldWind.ShapeAttributes(null);
            configuration.attributes.drawOutline = true;
            configuration.attributes.outlineColor = new WorldWind.Color(
                0.1 * configuration.attributes.interiorColor.red,
                0.3 * configuration.attributes.interiorColor.green,
                0.7 * configuration.attributes.interiorColor.blue,
                1.0);
            configuration.attributes.outlineWidth = 2.0;
        }
        else if (geometry.isPolygonType() || geometry.isMultiPolygonType()) {
            configuration.attributes = new WorldWind.ShapeAttributes(null);

            // Fill the polygon with a random pastel color.
            configuration.attributes.interiorColor = new WorldWind.Color(
                0.375 + 0.5 * Math.random(),
                0.375 + 0.5 * Math.random(),
                0.375 + 0.5 * Math.random(),
                0.5);
            // Paint the outline in a darker variant of the interior color.
            configuration.attributes.outlineColor = new WorldWind.Color(
                0.5 * configuration.attributes.interiorColor.red,
                0.5 * configuration.attributes.interiorColor.green,
                0.5 * configuration.attributes.interiorColor.blue,
                1.0);
        }

        return configuration;
    };

    $scope.getGeoJson = function (alpha3Code) {
        var resourcesUrl = "https://raw.githubusercontent.com/johan/world.geo.json/master/countries/" + alpha3Code + ".geo.json";

        // Polygon test
        var polygonLayer = new WorldWind.RenderableLayer("Polygon " + $scope.country);
        var polygonGeoJSON = new WorldWind.GeoJSONParser(resourcesUrl);
        polygonGeoJSON.load(null, $scope.shapeConfigurationCallback, polygonLayer);
        $scope.wwd.addLayer(polygonLayer);

    };

    $scope.getPollutedCities = function (result) {
        $scope.pollutedCities = {};
        $scope.pollutedCities.green = [];
        $scope.pollutedCities.yellow = [];
        $scope.pollutedCities.orange = [];
        $scope.pollutedCities.red = [];
        $scope.pollutedCities.purple = [];
        $scope.pollutedCities.burgundy = [];
        $scope.pollutedCities.black = [];

        var latAvg = 0;
        var longAvg = 0;
        var count = 0;

        $scope.cities = result.data.cities;
        angular.forEach($scope.cities, function (value, key) {
            $scope.object = {};
            $scope.object.cityName = value.city;
            $scope.object.latitude = value.station.g[0];
            $scope.object.longitude = value.station.g[1];
            $scope.object.pollution = parseInt(value.station.a);

            latAvg += $scope.object.latitude;
            longAvg += $scope.object.longitude;
            count++;
            if (count % 2 == 0) {
                latAvg /= 2;
                longAvg /= 2;
                count = 1;
            }

            if ($scope.object.pollution <= 50) {
                $scope.pollutedCities.green.push($scope.object);
            } else if ($scope.object.pollution <= 100) {
                $scope.pollutedCities.yellow.push($scope.object);
            } else if ($scope.object.pollution <= 150) {
                $scope.pollutedCities.orange.push($scope.object);
            } else if ($scope.object.pollution <= 200) {
                $scope.pollutedCities.red.push($scope.object);
            } else if ($scope.object.pollution <= 300) {
                $scope.pollutedCities.purple.push($scope.object);
            } else if ($scope.object.pollution <= 500) {
                $scope.pollutedCities.burgundy.push($scope.object);
            } else {
                $scope.pollutedCities.black.push($scope.object);
            }
            // $scope.placeMark($scope.object.cityName, $scope.object.latitude, $scope.object.longitude, $scope.object.pollution);
        });

        $scope.goToAnimator = new WorldWind.GoToAnimator($scope.wwd);
        $scope.goToAnimator.goTo(new WorldWind.Location(latAvg, longAvg))

        var start_index = 0;
        var number_of_elements_to_remove = 5;
        var length = $scope.pollutedCities.green.length;


        $scope.pollutedCities.green = $scope.pollutedCities.green.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.green, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, '028936');
        });

        length = $scope.pollutedCities.yellow.length;
        $scope.pollutedCities.yellow = $scope.pollutedCities.yellow.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.yellow, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, 'EDDB14');
        });

        length = $scope.pollutedCities.orange.length;
        $scope.pollutedCities.orange = $scope.pollutedCities.orange.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.orange, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, 'EB7F02');
        });

        length = $scope.pollutedCities.red.length;
        $scope.pollutedCities.red = $scope.pollutedCities.red.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.red, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, 'FF1C00');
        });

        length = $scope.pollutedCities.purple.length;
        $scope.pollutedCities.purple = $scope.pollutedCities.purple.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.purple, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, '6504CB');
        });

        length = $scope.pollutedCities.burgundy.length;
        $scope.pollutedCities.burgundy = $scope.pollutedCities.burgundy.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.burgundy, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, '890226');
        });


        length = $scope.pollutedCities.black.length;
        $scope.pollutedCities.black = $scope.pollutedCities.black.splice(start_index,
            length < number_of_elements_to_remove ? length : number_of_elements_to_remove);
        angular.forEach($scope.pollutedCities.black, function (value, key) {
            $scope.placeMark(value.cityName, value.latitude, value.longitude, '000000');
        });

    }

    var query = window.location.search.substring(1);
    var params = query.split("=")[1];
    $scope.country = params.replace("+", " ");
    console.log($scope.country);

    $scope.getCode = function (response) {
        // console.log(response.data);
        var countryInfo = response.data[0];
        var alpha3Code = countryInfo.alpha3Code;
        var alpha2Code = countryInfo.alpha2Code;

        $scope.getGeoJson(alpha3Code);
        $http.get("https://waqi.info/rtdata/ranking/" + alpha2Code + ".json").then($scope.getPollutedCities).catch($scope.logError);

    }

    $http.get("https://restcountries.eu/rest/v2/name/" + $scope.country).then($scope.getCode).catch($scope.logError);


});