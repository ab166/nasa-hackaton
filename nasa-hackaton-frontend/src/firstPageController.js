app.controller('firstPageController', function ($scope, $http) {
    $scope.logError = function (exception) {
        console.log("There was a failure retrieving the capabilities document: exception: " + exception);
    };

    $scope.pollution = 0;

    $scope.getPollutionDetails = function (response) {
        console.log(response.data.data.aqi)
        $scope.pollution = response.data.data.aqi;
    }

    $scope.getCurrentCity = function (response) {
        console.log(response.data.city)
        $http.get("http://api.waqi.info/feed/" + response.data.city + "/?token=b4b4817fd5ea705dc0950f406a99d476561e9cc1")
            .then($scope.getPollutionDetails)
            .catch($scope.logError);
    }

    $scope.getColor = function (level) {
        console.log('get color')
        if (level > 0 && level < 50)
            return 'green';

        if (level > 50 && level < 100)
            return 'yellow';

        if (level > 100 && level < 150)
            return 'orange';

        if (level > 150 && level < 200)
            return 'red';

        if (level > 200 && level < 300)
            return 'purple';

        if (level > 300 && level < 500)
            return 'burgundy';

        if (level > 500)
            return 'black';
    }

    $http.get("http://ipinfo.io").then($scope.getCurrentCity).catch($scope.logError);

});