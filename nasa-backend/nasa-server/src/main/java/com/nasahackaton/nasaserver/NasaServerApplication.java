package com.nasahackaton.nasaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NasaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NasaServerApplication.class, args);
	}

}
